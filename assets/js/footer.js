/* back to top button */
let mybutton = document.getElementById("back-to-top-button");

// call function every scroll event
window.onscroll = function() { scrollFunction() };
function scrollFunction() {
    let startShowAmount = 1000;
    let result = document.body.scrollTop > startShowAmount || document.documentElement.scrollTop > startShowAmount;

    if (result) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}

function backToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
/* end of back to top button */

/* accept cookies banner */
// This code is for the "do you accept cookies logic"
// If the person does not have a cookie saved in their browser
// the bottom banner will pop up

// grab a cookie value by the name of name/value pair
// you can access this from the web browser with 'document.cookie'
function getCookieById(cookieId){
    var name = cookieId + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for(var i = 0; i < ca.length; i++){
        var c = ca[i];
        while(c.charAt(0) == ' '){
            c = c.substring(1);
        }

        if(c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }

    return "";
}

// run the following code when the page finishes loading the HTML
// we do this to make sure the bottom banner is in memory and can be hidden if needed
document.addEventListener('DOMContentLoaded', (event) => {

    // save DOM reference for HTML element for banner and the "ok" button inside it
    let acceptBannerHTML = document.querySelector("#cookies-message");
    let acceptCookieButton = document.querySelector("#acceptCookieButton");

    // show the cookie banner if we did not accept it
    if(getCookieById("acceptedKexiCookiesPolicy") !== 'true') {
        acceptBannerHTML.setAttribute('style','display: block');
    }

    // create and store cookie for 1 year if they press the "ok" button on the bottom banner
    acceptCookieButton.addEventListener('click', function(){
        var cookieDate = new Date();
        cookieDate.setFullYear(cookieDate.getFullYear()+1);
        document.cookie="acceptedKexiCookiesPolicy=true; expires=" + cookieDate.toGMTString() + ";";
        acceptBannerHTML.setAttribute('style','display: none'); // hide banner since we accepted
    });
});
/* end of accept cookies banner */
