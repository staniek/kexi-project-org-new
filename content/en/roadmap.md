---
title: "Roadmap"
date: "2024-11-30"
layout: simple
---

KEXI has carved its niche as a powerful and intuitive visual database application, part of the KDE ecosystem and an answer to the needs of data enthusiasts, developers, and professionals. Initially envisioned as an accessible alternative to database tools like Microsoft Access, FileMaker, and LibreOffice Base, KEXI aimed to democratize database design, making it simple yet feature-rich for users across diverse skill levels. Over the years, the roadmap for KEXI has evolved, from “covering the basics” to “redefining what’s possible in open-source database solutions.”

In its earlier days, KEXI concentrated on establishing a robust foundation, addressing the core needs of creating, managing, and querying databases. But the vision was always more ambitious: KEXI wasn’t just about catching up to its proprietary peers. It was about offering a uniquely integrated, user-friendly experience in the open-source world. This vision has been shaped and refined through continuous feedback from the vibrant KDE community and beyond, ensuring that KEXI is aligned with the real-world needs of its users.

Now, as KEXI moves forward, the focus is on pushing boundaries. Enhancing multi-platform compatibility, refining usability for both casual users and database professionals, and incorporating cutting-edge features like advanced data visualization and cloud integration are all priorities. Through it all, KEXI remains grounded in its ethos: empowering users to take control of their data without compromise. This roadmap will provide clarity, continuity, and ambition as we chart KEXI’s course for the years to come.

### 2004

*(0.1 beta 2 ... beta 5)*

The first official 0.1 releases introduced significant advancements, including a newly rewritten SQLite-derived database engine (migrated from version 2 to 3), a tabbed user interface with detachable windows, support for schema-based queries, and fully internationalized error messages. Server database support for MySQL and PostgreSQL was also added, along with new command-line options.

Key enhancements were made to various tools:

- Table Designer: Introduced support for autonumbers.
- Data Table View: Improved navigation and data editing, with features like combo boxes for easier input.
- Query Designer: Offered both Visual and SQL modes, featuring a parser and support for criteria and date/time operations.

A general-purpose Properties window was added, along with a redesigned Query Designer and consistent error messages. This release also marked the debut of the initial version of the Form Designer and improved startup handling.

KEXI’s user interface began to be translated into a wider array of languages, making it more accessible globally. The project received crucial support during this period from the OpenOffice Polska company, which has continued its support ever since.

### 2005

*(0.8 beta 1 ... 0.8.1 / KOffice 1.4.1)*

KEXI received significant performance optimizations, user interface enhancements, and expanded functionality across its features. Key changes to core tools, such as Table, Query, and Form Designers, include hiding unsupported properties, improved better handling of design changes, and better support for data-aware widgets. The introduction of new form widgets, advanced property management enhances user creativity and usability. Additionally, database drivers and migration tools now support SQLite, MySQL, PostgreSQL, and limited Microsoft Access file imports.

In a notable milestone, KEXI's versioning was bumped from 0.1 to 0.8 to reflect the significant progress made in development. This release aligns with KOffice 1.4 for the first time, highlighting the growing maturity of the platform. Furthermore, the KEXI API Documentation was published for developers, with weekly updates to keep it aligned with ongoing changes. Error handling and user feedback have been refined across the board, as well as improved navigation keyboard shortcuts. Updates to the SQL Editor allow invalid queries to be edited and fixed by the users, while new table design synchronization features reduce potential data conflicts.

For database support, KEXI employs ISO 8601 date/time formats. A redesigned startup process allows even easier connectivity with shortcut files and command-line options. Specific tweaks for Windows and UNIX platforms improve compatibility and user experience, such as using "My Documents" as the default folder in File Dialogs and enabling local connections without requiring socket files on UNIX. These changes collectively make KEXI more accessible and native.

On August 29th, KEXI was introduced as a new database environment at the KDE Contributor and Developer Conference in Málaga, Spain. The same year the KEXI user and developer mailing lists were announced. The team also conducted an initial analysis comparing KEXI with other database applications, assessing its pros and cons in relation to Microsoft Access, OpenOffice.org Base, ADO.NET, and several smaller open-source alternatives.

(To be continued)

<!-- TODO
https://kexi-project.org/older_news.html
2006
2007
2008
2009
2010
2011
2012
2013
2014
https://kexi-project.org/about.html#news
2015
2016
2017
2018
2019
!-->
