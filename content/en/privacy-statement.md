---
title: "Privacy Statement"
date: "2024-11-28"
layout: simple
---

## Application Privacy Policy
KEXI values your privacy. No registration is required, and no internet connection is needed to install or use KEXI. It operates fully without requiring internet access.

KEXI does not access, collect, use, share, or transmit any personal information or user data. Since KEXI developers and supporters have no access to your data—sensitive or otherwise—they cannot and do not sell it.

Please note, however, that your operating system may track your usage of KEXI. For example, Windows logs all executables you run. While we may receive aggregated usage statistics, this data cannot be traced back to individual users.

Learn more about the [Privacy Policy for KDE applications](https://kde.org/privacypolicy-apps/).

## Website Privacy Policy

### Our Promise
The developers and supporters of KEXI are dedicated to protecting your privacy on *kexi-project.org*. No third parties will track you here, and you will not encounter third-party advertisements on this website.

### Cookies
*kexi-project.org* uses “cookies”—small text files placed on your computer—to manage registration and login processes and to collect anonymous standard internet log information and visitor behavior data.

We emphasize that *kexi-project.org* only uses benign first-party cookies.

First-party cookies are set solely by the website you are visiting. This means *kexi-project.org* controls the cookies entirely. We do not permit third parties to track you or set cookies on your device, nor do we host advertisements from external sources.

### Social media
Please note that embedded content from other providers may track your interactions. For example, clicking an embedded YouTube video will allow Google to track your activity. Similarly, using a social media “share” button will result in third-party tracking of that action.

Learn more about the general [KDE Privacy Policy](https://kde.org/privacypolicy/).
