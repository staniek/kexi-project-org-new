---
title: "Donations"
date: "2024-10-29"
layout: simple
---

<style>
    .donation-container {
      padding: 2rem;
    }
    p {
      margin-bottom: 2rem;
    }
</style>

KEXI is a Free and Open Source application, primarily developed by an international team of enthusiastic volunteers. We welcome donations from KEXI users to support our ongoing work!

<p>
<strong>At present, the KEXI project does not have a dedicated foundation to oversee its development.</strong>
</p>

<p>
You have two options:
</p>

<div class="row donation-container dark">
<h3>Sponsor individual KEXI developers</h3>
<p>
This option allows you to directly influence the areas of the project that will be developed. The developer will likely consider your specific requests. You can also order commercial services and sign a contract. 
</p>
    <form action="{{< ref contact >}}" method="post">
        <input type="submit" value="Contact developers" name="submit" class="primary-button"/>
    </form>
</div>

<p></p>

<div class="row donation-container dark">
<h3>Sponsor the whole organisation</h3>
<p>
Sponsor the broader organization, KDE, which supports the infrastructure and services shared by numerous software projects. This backing is essential for the smooth operation of the KEXI project.
</p>
    <form action="https://kde.org/community/donations/" target="_blank" method="post">
        <input type="submit" value="Donate to KDE" name="submit" class="primary-button"/>
    </form>
</div>

## Where does the money go?

- **Development** — The majority of funds are allocated here. Our goal is to support full-time developers, as this is the most effective approach. Their work involves fixing bugs, adding new features, and improving KEXI across all supported platforms. Development priorities are set in collaboration with our user community.
- **Hardware** — Some funds are used to provide developers with necessary test hardware.
- **Support** — Significant effort is dedicated to supporting users through news releases, interviews, forums, chat support, documentation, and cross-platform release builds.
- **Travel** — We fund travel to international conferences where KEXI is represented. Developer sprints are organized as short gatherings where KEXI developers and users collaborate in person.

## Sponsors

{{< sponsor-item imageUrl="images/sponsors/handshake-logo.svg" alt="Handshake Foundation" >}}

Handshake Foundation [donated](https://dot.kde.org/2018/10/15/kde-ev-receives-sizeable-donation-handshake-foundation) 300,000 USD to the KDE e.V. in 2018. The Calligra office suite project received 100,000 USD of that sum. Consequently, the Krita and KEXI projects, which are affiliated under Calligra, each received 10%.

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/sponsors/openoffice-polska-logo.png" alt="OpenOffice Polska" >}}

[OpenOffice Polska](https://openoffice.com.pl) LLC has employed KDE/KEXI developer Jarosław Staniek starting in March 2003 for a few years, allowing him to work full-time on KEXI and on porting KDE software libraries to [MS Windows](https://community.kde.org/Windows).

{{< /sponsor-item >}}

{{< sponsor-item imageUrl="images/sponsors/automatix-logo.png" alt="automatiX" >}}

[automatiX](https://www.automatix.de/index.php/en) GmbH sponsored hosting for the kexi-project.org website for many years.

{{< /sponsor-item >}}

## Updates and product roadmap

We post development updates and status reports on our homepage. If you want to be notified when new releases come out you can sign up for our release mailing list. The mailing list only sends out an email when final builds are done. If you want to see early builds, you will need to catch the news.

You might not want to constantly check the website for updates. You can use [our RSS feed](/index.xml) to subscribe to news updates. 

<!-- ## Having issues with donating?

KEXI is located in ___. You might have to contact your bank before a payment can go through. For the USA, you might have to put a "travel notice" in for the Netherlands for this to work.

If you continue having issues, please [contact us](/contact/) so we can learn more about your issue. 
-->

## Large Donations

If you are in the EU, or if you want to donate larger sums, it will usually be best to use a direct bank transfer, to avoid Paypal’s fees. For questions, <span class="nowrap">[contact us](/contact/)</span>.


## Tax Deductible

<a href="https://ev.kde.org/">KDE e.V.</a> is the non-profit organization behind the KDE community. It is based in Germany and thus donations are tax deductible in Germany. If you live in another EU country your donation might also be tax deductible. Please consult your tax advisor for details.

If you choose to pay a KEXI developer through a contract, you will receive an invoice that you can account for in your company.
