---
title: "License"
date: "2024-11-27"
layout: simple
---

# License for usage

## The App

{{< i18n "free-opensource-text" >}}
<br>

This license grants you the following rights:

- To use the software for any purpose, including commercial or personal work, as well as installation in schools or companies.
- To modify the software and create derivative works.
- To distribute original or modified (derivative) works under the terms of this license or any newer version.
- To grant or extend a license to the software.

You cannot:
- Be held liable for damages.
- Statically link libraries into a program.

When distributing the software, you must:
- Provide copies of the original software or instructions on how to obtain them.
- Retain the original copyright.
- Include the full text of the license in the modified software.
- Disclose the source code.
- State any significant changes made to the software.
- Include a "NOTICE" file with attribution notes.

All of the above obligations must be fulfilled without delay.

Components such as libraries used by KEXI may be licensed under more permissive licenses. This is indicated in each source file. Our source code specifies where you can obtain the source for these components and includes any changes we have made to them.

## Sharing or Selling Plugins
KEXI can be extended with plugins. The extension API is an integral part of KEXI and is licensed under the [GNU Library General Public License](http://www.gnu.org/copyleft/lesser.html) (LGPL), version 2. This means that if you distribute a KEXI plugin it, it must comply with the terms of this license.

## Your Projects
What you create with KEXI is entirely your property. You are free to use your projects however you like, with no restrictions on how you utilize KEXI. 

This means KEXI can be used for commercial purposes, scientific research, or educational activities by students and institutions alike.

KEXI’s GNU LGPL license guarantees this freedom. Unlike trial or "educational" versions of other software that may restrict commercial use, KEXI ensures your right to use your work without limitations.

# Websites

## Marketing Website — kexi-project.org
This is the central place where people can learn about KEXI and download the latest version. The website is hosted by KDE, and the source code is maintained on KDE's GitLab service at [invent.kde.org](https://invent.kde.org/websites/kexi-project-org). It is licensed under the terms of the [GNU Free Documentation License](https://docs.kde.org/trunk5/en/kdoctools5-common/fdl-license.html).

## Documentation Websites — docs.kde.org
This is the educational section hosting KEXI's handbook, licensed under the terms of the [GNU Free Documentation License](https://docs.kde.org/trunk5/en/kdoctools5-common/fdl-license.html).

## Development Wiki — community.kde.org/Kexi
This website is dedicated to the development of KEXI and is licensed under the terms of the [Creative Commons License SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## Website Credits
- Original Design and Development (krita.org) – Scott Petrovic
- Design and Development (kexi-project.org) – Jarosław Staniek
