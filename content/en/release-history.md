---
title: "Release History"
date: "2024-12-03"
layout: simple
---

<div class="release-history">

This timeline highlights the history of KEXI, tracing its development and milestones since 2004.

[2025](#2025) • [2024](#2024) • [2019](#2019) • [2018](#2018) • [2017](#2017) • [2016](#2016) • [2015](#2015) • [2014](#2014) • [2013](#2013) • [2012](#2012) • [2011](#2011) • [2010](#2010) • [2009](#2009) • [2008](#2008) • [2007](#2007) • [2006](#2006) • [2005](#2005) • [2004](#2004)

### 2019

* <pre>3.2 Beta</pre> <a href="https://community.kde.org/Kexi/Releases#3.2" target="_blank">Released on 2019-01-21</a>
* <pre>3.2  </pre> <a href="https://community.kde.org/Kexi/Releases#3.2" target="_blank">Released on 2019-04-09</a>, improving stability of the app and frameworks. Since version 3.1.0, KEXI and the frameworks have received about 80 improvements and bug fixes. KEXI has greatly improved support for Date, Date/Time, and Time types. Startup improvements refine resource lookup and translations, while updates to the Welcome Page and Table/Form Views ensure proper year formats, integer value display, and robust input validation. Query and report functionalities have been stabilized with crash fixes, parameter support, and better error handling. Import/export processes have been enhanced with improved MS Access data handling, increased binary object size limits, and crash prevention during data transfers.

### 2018

* <pre>3.1 Beta</pre> <a href="https://community.kde.org/Kexi/Releases#3.1" target="_blank">Released on 2018-01-29</a>
* <pre>3.1 RC</pre> <a href="https://community.kde.org/Kexi/Releases#3.1" target="_blank">Released on 2018-02-25</a>
* <pre>3.1   </pre> <a href="https://community.kde.org/Kexi/Releases#3.1" target="_blank">Released on 2018-03-10</a>, improving overall stability and preparing the KEXI frameworks, KDb, KProperty, KReport, for general use in other (non-KEXI) software projects. Since the 3.0.2 release KEXI and its frameworks received over 220 improvements and bug fixes.

### 2017

* <pre>3.0.1</pre> <a href="https://community.kde.org/Kexi/Releases#3.0" target="_blank">Released on 2017-03-13</a>, fixing a few minor memory problems and adding more icons to the standard theme. Fixed Table Designer's  altering physical table design, and relationships creation using drag & drop in Query Designer.
* <pre>3.0.2</pre> <a href="https://community.kde.org/Kexi/Releases#3.0" target="_blank">Released on 2017-08-11</a>, fixing a number of general user interface and usability issues. Some fixes for version 3.0.0. Further improvements to Query Designer, and for using queries in forms and reports. Improved support for non-KDE Plasma desktops such as XFCE, and for MS Windows. Auto-opening command line options work again.

### 2016

* <pre>2.9.11</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.11" target="_blank">Released on 2016-02-03</a>, featuring a commercially developed upgrade for MS Access (MDB) import to KEXI, donated by Jarosław Staniek. Import functionality, the Global Search, property editor, and toolbar visuals have been improved. Table Views now feature better error handling, resolving issues in queries, forms, reports, and CSV exports. Query handling fixes visibility issues, supports multiple tables with the same field name, and improves performance. Forms now support sorting, and Reports address aggregate value retrieval and latitude property handling in map elements. Memory handling has also been significantly improved.
* <pre>3.0 Alpha</pre> <a href="https://community.kde.org/Kexi/Releases#3.0" target="_blank">Released on 2016-09-14</a>
* <pre>3.0 Beta</pre> <a href="https://community.kde.org/Kexi/Releases#3.0" target="_blank">Released on 2016-09-20</a>
* <pre>3.0  </pre> <a href="https://community.kde.org/Kexi/Releases#3.0" target="_blank">Released on 2016-10-05</a>, built on top of KF 5 and Qt 5, introduces a new PostgreSQL driver, significant improvements in database migration for MS Access, MySQL, PostgreSQL, and SQLite. Most icons adopted the Breeze theme. Command-line options have been restored, issues for Date/Time values and masks fixed. The Welcome page's behavior has been refined for smoother startup.

### 2015

* <pre>2.9 Beta 2</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9_Beta_2" target="_blank">Released on 2015-01-15</a>
* <pre>2.9 Beta 3</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9_Beta_3" target="_blank">Released on 2015-02-13</a>
* <pre>2.9  </pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9" target="_blank">Released on 2015-02-26</a>, focused on improving the quality of existing features. Thanks to close collaboration with users of KEXI, about 150 changes have been performed. Fixed editor sizes in Table Views, combo box popups, vertical header updates, insertion, and cursor movement. Usability improvements ensure smoother scrolling during edits. Row editing and the scrollbar behavior has been optimized. Column width management has been improved in queries, as well as support for the LIKE operators. Reports now support Longitude, Latitude, and Zoom properties for maps. Import/Export functionality is optimized, including fixes for OpenDocument Spreadsheet files import and export of parameterized queries to CSV.
* <pre>2.9.1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.1" target="_blank">Released on 2015-03-15</a> featuring clear buttons in Object Name dialogs, consistent tab titles, and fixed status bar updates across partitions. Database handling ensures proper .kexi file compacting. Table Designer now hints when no field is selected. Queries support SUBSTR and maintain correct dirty flag states. Reports add map theme selection, editing shortcuts, and proper Cut action handling.
* <pre>2.9.2</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.2" target="_blank">Released on 2015-04-02</a> resolved configuration errors related to the Marble maps component. Combo boxes in table and form views now display a single column, with proper record display in data-aware fields. Forms address crashes when removing elements in the object tree.
* <pre>2.9.3</pre> Cancelled on 2015-04-29.
* <pre>2.9.4</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.4" target="_blank">Released on 2015-05-06</a> featuring proper closing windows when objects are overwritten after "Save As," removing the "dirty" flag when saving modified objects, and saving the Find dialog's position and size. The main window is correctly removed upon closing. The Design tab is activated when needed on switching to design mode. In the Query Designer users can now switch from an empty Design view to SQL view, and clearer error messages are shown when switching empty views. Fixed text editor crashes caused by loading issues. Type handling for widget properties is fixed. The map widget fixes the latitude and longitude values for better precision and translation. A list of themes is available, and the property editor’s spin box is accessible as a slider. Report elements are updated, with correct encoding for unit names and improved map property handling. CSV import has improved column name editing, the ability to edit the first row, and better handling of empty column names.
* <pre>2.9.5</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.5" target="_blank">Released on 2015-06-09</a> fixing the issue of opening two Project windows from the Welcome page. In queries, the ILIKE/NOT ILIKE operator is now supported for PostgreSQL, and a crash when a lookup table is aliased has been resolved. For Reports, the data source combo box usability was improved, sorting fixed, as well as field selection, and empty items. The Maps element now supports setting properties via scripts, passing two or three fields of data to an item.
* <pre>2.9.6</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.6" target="_blank">Released on 2015-07-10</a> fixing the Welcome status bar GUIs and adjust recent project captions. The global search box is visuals are corrected. Crashes caused by incorrect command line arguments have been fixed. In Queries, a crash in result handling is resolved. SQLite database compacting now correctly renames files to their original name. A crash when importing PostgreSQL databases to a file project is fixed.
* <pre>2.9.7</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.7" target="_blank">Released on 2015-09-03</a> fixing the vertical alignment of text in command link button widgets and addressing issues from KEXI 2.9.5 and 2.9.6 regarding the ability to alter table designs. KEXI no longer forces saving queries when switching an unstored query to Data view. CSV import has improved primary key column detection. For SQLite databases, prepared statements now have improved results and error reporting.
* <pre>2.9.8</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.8" target="_blank">Released on 2015-10-09</a> fixing the issue of repeatedly opening the same object, particularly for large data. In Tables, the combo box list now ensures visibility of the highlighted row, and better handles scrolling. Queries add support for 22 scalar SQL functions such as ABS and FLOOR, that work across SQLite, MySQL, and PostgreSQL. This brings unified SQL support to KEXI. The function parser also handles argument checking, error messages, and extensibility. Support for hexadecimal literals in BLOB operations has been added, as well as optimization of expression type evaluation, and improved parsing of floating-point constants, BLOB literals, and argument lists. For MySQL, library versions are better detected. PostgreSQL has improved returning of NULL values and NULL columns types and using "BYTEA HEX" escaping for native statements.
* <pre>2.9.9</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.9" target="_blank">Released on 2015-11-05</a> improving shortcut for inserting new lines in Table and Query Designers. A crash when executing Cut, Undo, Cut in a form widget has been fixed. KEXI also builds correctly when using SQLite versions older than 3.8.3.
* <pre>2.9.10</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9.10" target="_blank">Released on 2015-12-09</a> improving SQL handling to allow "BETWEEN ... AND" and "NOT BETWEEN ... AND" to work with NULL arguments.
### 2014

* <pre>2.8  </pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.8" target="_blank">Released on 2014-03-05</a>, focused on improving the quality of existing features. Thanks to close collaboration with users of KEXI, about 30 issues has been identified and fixed. The only new feature is ability to open hyperlinks in forms. But the big news is that the app is now available to build and run on Windows for the first time in 2.x series.
* <pre>2.8.1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.8.1" target="_blank">Released on 2014-03-28</a> featuring support for 'NOT LIKE' in Query Designer and retaining required table prefixes. Reports avoid crashes in non-writable directories, scripting is enabled experimentally, and CSV import sorting is improved.
* <pre>2.8.2</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.8.2" target="_blank">Released on 2014-04-16</a> fixing the Text Editor form widget which now defaults to Rich Text off to prevent misbehavior.
* <pre>2.8.3</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.8.3" target="_blank">Released on 2014-05-15</a> fixing issues causing crashes when closing windows or modifying fields and improving resource handling. Query Designer now includes table context menus, form widgets support background colors, and reserved SQL keywords are updated, allowing their use as column names.
* <pre>2.8.5</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.8.5" target="_blank">Released on 2014-07-05</a> with fixes for crashes during database connection removal and duplicate table queries. Table views retain their position on first display, reports show correct page numbers, and search keywords persist. Newly added tables and queries are sorted in data sources, Ctrl+S is supported across designers, and resource leaks are resolved. Map elements in reports are now reliably retrieved.
* <pre>2.8.6</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.8.6" target="_blank">Released on 2014-09-24</a> featuring improved visibility of editing indicators in tabular and form views, disabled report navigation buttons on the last page, and crash fixes in multiple areas. Enhancements include offset positioning for pasted report elements, refined toolbar behavior, and a dedicated password dialog. System PostgreSQL and MySQL databases are now hidden, unicode text sorting is consistent, and the Database Import Assistant offers smoother password handling, directory management, and connection switching.
* <pre>2.8.7</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.8.7" target="_blank">Released on 2014-12-03</a> featuring an improved main menu, macOS build fixes, password prompts for connection tests, and file overwriting checks. Memory leaks and uninitialized variables were fixed. The tabbed toolbar now supports more styles. In Forms, unnecessary properties were hidden and shortcuts fixed. Report improvements include easier element creation, better size handling, and fixes for the toolbox and chart plugin. The Report Designer now has improved styling and a fixed ruler.
* <pre>2.9 Beta 1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.9 Beta" target="_blank">Released on 2014-12-14</a> featuring usability improvements and bug fixes, including the porting of Forms from Qt 3 to Qt 4. Bug reporting is simplified with auto-filled OS and platform info. Side panes are lighter, with a new "Close all tabs" action. The main tabbed toolbar's appearance is enhanced for GTK+ and Oxygen styles. Permissions for database creation are better handled to prevent unsafe paths. KEXI now exits gracefully when plugins are missing. Right-to-left UI support is fixed, and the "Save password" checkbox is simplified. Redundant actions have been removed. Reports now support inline label editing, prompts for opening exported documents, and high DPI printing.

### 2013

* <pre>2.4.4</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.4.4" target="_blank">Released on 2013-01-23</a> fixing the palette background color property in the text editor, and text box data source tag. SQLite Driver compacted file data loss was resolved, the 101-field limit in Table Designer removed, MySQL login without saved passwords fixed, and a crash using the tab key in the global search box addressed.
* <pre>2.5.5</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.5.5" target="_blank">Released on 2013-01-23</a> ensuring the description is displayed in Form Command Link buttons, sorting items by key in the property editor, addressing MySQL login failures when not saving passwords, and resolving a potential crash when using the tab key in the global search box.
* <pre>2.6  </pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.6" target="_blank">Released on 2013-02-05</a> added new support for user data storage, improved CSV import and export and improved table views. Overwriting objects with the same name is now possible.
* <pre>2.6.1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.6.1" target="_blank">Released on 2013-02-20</a> improving options for the CSV Export (text quote and encoding), fixing the "Default" button function, and preventing the "Cancel" button from accepting changes.
* <pre>2.6.2</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.6.2" target="_blank">Released on 2013-03-13</a>. Forms now prevent crashes when creating a new object form and closing without saving.
* <pre>2.6.3</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.6.3" target="_blank">Released on 2013-05-15</a> fixing look of the modern menu and tabbed toolbar for the bespin widget style.
* <pre>2.7  </pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.7" target="_blank">Released on 2013-08-01</a> improves CSV data import, in particular now it can import data into an existing table. Passwords do not require modal dialogs and are better handled.
* <pre>2.7 Beta</pre> Released on 2013-05-08 featuring a import CSV data into an existing table.
* <pre>2.7.2</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.7.2" target="_blank">Released on 2013-08-26</a>
* <pre>2.7.3</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.7.3" target="_blank">Released on 2013-10-11</a>, fixing sorting errors with NULL and text values in table views, proper text escaping for PostgreSQL, improved field insertion in Table Designer, and restored property display in Table Designer.
* <pre>2.7.4</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.7.4" target="_blank">Released on 2013-10-17</a> fixing saving new table designs without clearing edit history.
* <pre>2.7.5</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.7.5" target="_blank">Released on 2013-11-27</a> ensuring query design changes reflect in reports, records sort by query order, improved command-line stability for missing drivers, and fixed password dialog regression.

### 2012

* <pre>2.4 Beta 6</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.4_Beta_6" target="_blank">Released on 2012-01-11</a> featuring stability improvements, Calligra branding, better dark theme, and a streamlined feedback agent. Design tabs now adjust dynamically. Fixes address crashes in Forms and CSV imports, improve Image Box behavior, and ensure proper query generation with KEXISQL. SQLite drivers now use ICU for enhanced Unicode sorting.
* <pre>2.4  </pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.4" target="_blank">Released on 2012-04-12</a> with extensive updates, including terminology clarifications and improved plugin loading. The application handles edge cases more gracefully, such as confirming pending changes before switching views. Usability is enhanced with a modernized menu, improved search, and navigation. Stability in database drivers, query handling, and record management has been strengthened. Database support was expanded with driver updates, better compatibility, and sorting. New developer contributions and updated documentation are included.
* <pre>2.4.1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.4.1" target="_blank">Released on 2012-04-25</a> featuring a fix for field name changes via the Property Editor, and a fix in SQLite database compacting.
* <pre>2.5 Beta</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.5_Beta" target="_blank">Released on 2012-06-23</a> featuring full-screen mode and new form widgets Command Link Button, Slider, Progress Bar, and Date Picker.
* <pre>2.4.3</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.4.3" target="_blank">Released on 2012-06-27</a> featuring a fixed CSV import with changes to the "Start at Line" value, a 255-character limit for Text type, and removal of limits for the Text data type. Additionally, issues with data saving related to widget focus policy were fixed.
* <pre>2.5  </pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.5" target="_blank">Released on 2012-08-13</a> featuring a full screen mode and new form elements, improved icons, Table Designer enhancements, CSV import functionality improved, graphics enhancements in Reports, standardized UI texts for export functions.
* <pre>2.5.1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.5.1" target="_blank">Released on 2012-08-29</a> featuring a fix for the Oxygen style in the Modern Menu, along with resolved CSV import crashes. The missing metadata no longer causes critical errors, and new project files are saved in the correct folder. Table View now shows warnings for invalid dates, with improved validation popups. Several checks have been added to the configuration and build system.
* <pre>2.5.2</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.5.2" target="_blank">Released on 2012-09-13</a> featuring removal of the non-functional SQL Editor history in Query Designer and adding string concatenation support. Startup UI improvements and the option to disable specific database drivers during KEXI build are also added.
* <pre>2.6 Beta</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.6_Beta" target="_blank">Released on 2012-10-31</a> improving the "Rename" action in Project Navigator, and support for overwriting objects with the same name.
* <pre>2.5.3</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.5.3" target="_blank">Released on 2012-11-06</a> featuring enforced lowercase identifiers, supports improved SQLite reliability and importing uppercase table names. Crashes on closing are fixed, tab bars display names consistently, and Table Designer ensures stable saves. Query Designer restores "All Columns" support.
* <pre>2.5.4</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.5.4" target="_blank">Released on 2012-11-21</a>. In this release the field limit in the Table Designer has been removed, and multiple issues in Forms addressed: rich text formatting, colors and text boxes.

### 2011

* <pre>2.4 Beta 1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.4_Beta_1" target="_blank">Released on 2011-09-14</a> featuring a new modern menu and startup screen, starting a refreshed look for the app.
* <pre>2.3.1</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.3.1" target="_blank">Released on 2011-01-19</a>. Several memory leaks were fixed, and the MySQL driver now supports local socket path setting. KEXI no longer crashes if no table is selected for import, and the migration wizard correctly saves recently visited directories for source and destination databases.

<br/>

-----

## Releases With KOffice

KEXI was part of <a href="https://en.wikipedia.org/wiki/KOffice" target="_blank">KOffice</a> until 2010-12-06, when it <a href="https://calligra.org/news/calligra-suite-goes-active/" target="_blank">transitioned to the Calligra Suite</a>.

<br/>

### 2010

* <pre>2.3  </pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.3" target="_blank">Released on 2010-12-31</a> The CSV Export dialog has been enhanced, and reports can now be exported to word processors. A plugin framework has been introduced for reports, offering greater flexibility. Object tabs in the main window are simpler and movable, and a redesigned Project Navigator pane has been added. Images in forms now scale more smoothly, and the Widgets Tree in the Forms Designer has been reinstated. Support for MS Access files has been improved, alongside enhancements to the PostgreSQL and SQLite drivers.

* <pre>2.2  </pre> Released on 2010-05-28 featuring KEXI again in KOffice for the first time since 2006 (KOffice 1.6 i.e. KEXI 1.1), being a complete overhaul of the user interface. This means over 3 years of developement.

* <pre>2.2.1</pre> Released on 2010-07-15, improved performance for image handling in forms and fixed title display issues in drop-down menus. The "open object by name" action for form buttons was corrected, and the usability of the migration dialog was enhanced. Additionally, issues with auto-opening and a potential crash was resolved.

* <pre>2.2.2</pre> Released on 2010-08-27, fixed bug caused by reports not being saved before display.

* <pre>2.3 Beta</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.3_Beta" target="_blank">Released on 2010-09-16</a>. KEXI's production readiness depends on user needs. Since version 2.2, stability has improved, but some 1.x features, are still missing or require rework.

### 2009

* <pre>2.0 Alpha 12</pre> <a href="https://community.kde.org/Kexi/Releases/Kexi_2.0_Alpha_12" target="_blank">Released on 2009-04-09</a>, providing extensive update which includes clarified terminology and faster plugin loading. It now handles edge cases more smoothly, such as pending changes or invalid identifiers. Usability improvements include a refined global search box, updated Project Navigator, and a modernized menu. Stability has been enhanced across database drivers, queries, and record management, while expanded driver support addresses compatibility and sorting issues. Contributions from new developers and updated documentation further enrich the platform.

### 2008

* <pre>2.0 Alpha 7 </pre> Released on 2008-05-07
* <pre>2.0 Alpha 8 </pre> Released on 2008-05-09, featuring CSV Import Dialog ported to KDE 4
* <pre>2.0 Alpha 9 </pre> Released on 2008-07-07
* <pre>2.0 Alpha 10 </pre> Released on 2008-07-12, featuring improvements in the CSV Import Dialog - boolean type and better date type support

### 2007

* <pre>1.1.2</pre> <a href="https://community.kde.org/Kexi/Releases/1#1.1" target="_blank">Released on 2007-02-21</a> featuring a new command-line option to display the Project Navigator in User Mode. The project manager now executes macros and scripts with a click. Older KEXI projects can open in read-only mode, and queries can serve as row sources for look-up fields with multiple visible columns supported. Query results sort by primary key when no explicit sorting is set, and SQL statement construction for multi-table queries is improved.
* <pre>1.1.3</pre> <a href="https://community.kde.org/Kexi/Releases/1#1.1" target="_blank">Released on 2006-10-16</a> featuring improved behavior in "Search all rows" mode. The SQL parser supports recursion, resolving crash issues. Table view fixes improve navigation for "Image" cells, and query-based data sources enforce read-only columns. The Table Designer no longer warns unnecessarily when switching to data view. Proper relation is displayed in Query Designer, without internals. Forms now support drag-and-drop for containers, tab key handling is improved, and popup behavior refined in combo boxes and image boxes. Printouts resolve issues with page boundaries, preview updates, and landscape mode.

### 2006

* <pre>1.0 Beta</pre> <a href="https://community.kde.org/Kexi/Releases/1.0.0_Beta_1_Changes" target="_blank">Released on 2006-01-31</a>
* <pre>2006 Demo for Windows</pre> Released on 2006-03-06 (commercially supported)
* <pre>1.0  </pre> <a href="https://community.kde.org/Kexi/Releases/1#1.0" target="_blank">Released on 2006-04-11</a> code named "Black Mamba", introduces significant enhancements, including improved data-aware forms with a Data Source Pane and an object tree view for widget navigation. Data handling is streamlined with CSV import/export and clipboard integration. Forms now support stored images and include a new multiline editor. Database imports have been refined for smoother migration from server databases to files and full KEXI project imports. Extended scripting plugin for Python and Ruby, and  basic print support for data. This release features over 200 fixes and improvements, with contributions from OpenOffice Polska.
* <pre>1.0.1</pre> <a href="https://community.kde.org/Kexi/Releases/1#1.0" target="_blank">Released on 2006-05-22</a>, enhancing SQL support and corrects date entry issues, while improving error and warning handling, design change saving, and crash prevention. Includes multiple upgrades to the CSV Import Dialog and resolves bugs in the Form Designer and Property Pane.
* <pre>1.0.2</pre> <a href="https://community.kde.org/Kexi/Releases/1#1.0" target="_blank">Released on 2006-07-14</a>, improving the CSV Import, and clipboard import. More efficient property handling, and Table View enhancements for row insertion and deletion. Resolved issues with database paths. Fixed loading form plugins, set data sources in queries, and fixed saving data for database-aware checkboxes.
* <pre>1.1 Alpha</pre> <a href="https://community.kde.org/Kexi/Releases/1#1.1" target="_blank">Released on 2006-08-01</a>
* <pre>1.1 Beta</pre> <a href="https://community.kde.org/Kexi/Releases/1#1.1" target="_blank">Released on 2006-09-10</a>
* <pre>1.1  </pre> <a href="https://community.kde.org/Kexi/Releases/1#1.1" target="_blank">Released on 2006-10-16</a> code name "Access Granted", with over 270 improvements. Added image handling, database compaction, automatic datatype recognition, and scripting tools. Also improved usability.
* <pre>1.1.1</pre> <a href="https://community.kde.org/Kexi/Releases/1#1.1" target="_blank">Released on 2006-11-29</a>, with a new combo box utilizing database relationships and parameter queries. These user-requested features simplify database solution development.

### 2005

* <pre>0.8  </pre> <a href="https://community.kde.org/Kexi/Releases/1#0.8" target="_blank">Released on 2005-06-21</a> together with KOffice 1.4.0, the first KOffice release to officially include KEXI. (The switch to 0.8 version was due to long list of improvements in this release and in response to user requests)
* <pre>0.8.1</pre> <a href="https://community.kde.org/Kexi/Releases/1#0.8" target="_blank">Released on 2005-07-26</a> together with KOffice 1.4.1, fixing compilation issues and resolving a startup crash. Label widgets now properly display shadows.
* <pre>0.9 Beta</pre> Released on 2005-05-09
* <pre>0.9  </pre> <a href="https://community.kde.org/Kexi/Releases/1#0.9" target="_blank">Released on 2005-05-31</a>, introducing major database enhancements, making the SQLite driver the default and adding official PostgreSQL support while refining the MySQL support. Redesigned the Data Table interface alongside improvements to the Table and Query editors. New startup dialogs and wizards introduced as well as migration between database engines, such as SQLite to MySQL and MS Access to SQLite. The user interface was stabilized throughout.
* <pre>1.0 Preview for Windows</pre> Released on 2005-06-14
* <pre>1.0 Preview #2 for Windows</pre> Released on 2005-10-18

### 2004

* <pre>0.1 Beta 2</pre> <a href="https://community.kde.org/Kexi/Releases/0.1.0_Beta_2_Changes" target="_blank">Released on 2004-01-20</a>, code named "Warsaw By Night", featuring own SQLite-derived engine (KEXISQL) and running without dependency on KOffice libraries. KEXI offers quick links for database servers and supports tabbed, childframe, toplevel, and IDEAl MDI UI modes. The rewritten database library features a cleaner API and schema-based query support. The SQL parser can optionally support internationalized messages.
* <pre>0.1 Beta 3</pre> <a href="https://community.kde.org/Kexi/Releases/0.1.0_Beta_3_Changes" target="_blank">Released on 2004-05-01</a>, code named "United Europe", improving Table Designer with a new Properties window. The Query Designer is renewed. A new "Data" menu allows saving the current table row. Main window actions are updated, and large binary/text objects can be stored in the database. KexiDB library improvements.
* <pre>0.1 Beta 4</pre> <a href="https://community.kde.org/Kexi/Releases/0.1.0_Beta_4_Changes" target="_blank">Released on 2004-07-16</a>, code named "FireDuck", improving query editing and form designer previews. The Form Designer now supports widget resizing, Ctrl-based copying, and improved pixmap management. Connections can be made via a new dialog or drag-and-drop, and form styles are selectable via the toolbar. Label buddy widgets and editable TabStops are also supported. The Query Designer saw major improvements, and fixes were made in the Table Designer, Data Table View, and overall app functionality, including error handling and data management.
* <pre>0.1 Beta 5</pre> <a href="https://community.kde.org/Kexi/Releases/0.1.0_Beta_5_Changes" target="_blank">Released on 2004-11-01</a>, code named "Halloween", featuring tens of improvements in KexiDB, SQLite3 support, improved Form Designer, Table Designer, and Query Designer.
* <pre>0.1 Beta 5.1</pre> <a href="https://community.kde.org/Kexi/Releases/1#0.1" target="_blank">Released on 2004-11-08</a>. Until the 2006 Windows Demo, all releases included versions for both Linux and Windows.

</div>
