---
title: "KEXI 3.1.0 Released"
date: "2018-03-09"
categories: 
  - "news"
  - "officialrelease"
---

__KEXI 3.1.0 Released with Improved Stability and Frameworks! KEXI 3.1.0 focuses on stability and readiness of frameworks—KDb, KProperty, and KReport—for use beyond KEXI. Since 3.0.2, over 220 improvements and fixes have been made.__

## Highlights

Integration of KEXI with 3rd-party (non-[KDE Plasma](https://kde.org/plasma-desktop)) desktops has been further improved. On such desktops (e.g. XFCE, MS Windows) KEXI now displays simplified file requester. It is also possible to use this feature on Plasma desktops. Moreover, settings of a single-click mode are better detected on 3rd-party desktops.

![Simplified file requester in KEXI 3.1](images/posts/2024/kexi-3.1.0-simple-file-dialog.png)

KEXI 3.1 uses all the three KEXI frameworks and the version 3.1 is a minimal version required by to run the app.

API of the KEXI Frameworks is now guaranteed to be stable until the 4.0.0 release. Documentation of the Frameworks has also improved and is available at [api.kde.org](https://api.kde.org/kexiframeworks). Users of the Frameworks will also notice improved translations of messages.

## Missing features

KEXI user's manual is outdated as it covers older versions 2.x. We're sorry for the inconvenience.

Also, tutorials and reference for KEXI Frameworks is expected in coming releases.


## Details & Download

For detailed list of changes, and downloads, visit the development page: https://community.kde.org/Kexi/Releases#3.1.

This release offers source code downloads and installers for 64bit MS Windows. For versions for other systems ask your Linux distributor. The source code is known to build and run with Linux (gcc, clang) and MS Windows (msvc>=2013). Some binaries such as installers for MS Windows 64bit are planned for RC or stable versions.

{{< see-download >}}

{{< see-release-history >}}
