---
title: "KEXI 3.1.0 RC 1 Released"
date: "2018-02-25"
categories: 
  - "news"
  - "officialrelease"
---

__KEXI 3.1.0 RC 1 Released with Improved Stability and Frameworks! KEXI 3.1.0 focuses on stability and readiness of frameworks—KDb, KProperty, and KReport—for use beyond KEXI. Since 3.0.2, over 220 improvements and fixes have been made.__

See the announcement of <a href="/en/posts/2024/kexi-3.1.0-beta1-released/">Beta 1</a> for highlights and details since 3.0.2.

Since 3.1.0 Beta 1, social buttons have been added to KEXI menu. KEXI examples are now built and installed by default as it's likely expected by users. There are also several bug fixes.

For detailed list of changes, and downloads, visit the development page: https://community.kde.org/Kexi/Releases#3.1.

{{< see-download >}}

{{< see-release-history >}}
