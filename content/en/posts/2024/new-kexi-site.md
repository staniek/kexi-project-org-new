---
title: "KEXI-Project.org Has a New Website!"
date: "2024-12-19"
categories: 
  - "news"
  - "officialrelease"
---

__After months of work, KEXI has a new website! The old was very old both in terms of content and design, and not very active. Over years there were many bits of information in the Development wiki pages instead of the home website.__

To address this, all existing content was carefully analyzed. Some of it was moved to the development wiki, some remained on the website, and some was removed altogether. This process ensured that the information reflected the project's current state, plans, and capabilities. The work began back in early 2023.

![KEXI Website: New vs. Old](images/posts/2024/new-kexi-site.webp)

The new website's design is adapted from the [Krita.org website](https://invent.kde.org/websites/krita-org), which was created using the Hugo static site generator—an excellent tool for building websites like this. Several adjustments were made to tailor the design to the specific needs of the KEXI project. The version used as the base for KEXI's site is from November 2024.

The design of the web site was adapted from the . It was built using the Hugo static site generator, very useful for building such websites. Several modifications have been made to meet the specific needs of the KEXI project. The latest version borrowed from Krita is from November 2024.

Rebuilding the site was a time-intensive effort, taking resources that could have otherwise gone into KEXI development. To support this important update, part of the funds donated by the <a href="{{< ref donations >}}#sponsors">Handshake Foundation</a> were used. A great deal of effort went into making this new website a reality, with the work done by the KEXI maintainer, Jarosław Staniek.

## What's next?

The new website is now capable of supporting translations into other languages, which may be introduced in the near future. Plans also include extending the website and improving integration with the extensive documentation required for a complex application like KEXI.

All the files for the website are <a href="{{< ref "/license" >}}#websites">open-source</a>. Detailed information about the website's development process is available on the [development GitLab instance](https://invent.kde.org/websites/kexi-project-org).

## Thanks!

Special thanks go to everyone involved in this transition, the Handshake Foundation, and the contributors to the original (Krita) website infrastructure: Scott Petrovic, Phu Nguyen, Wolthera van Hövell, Alvin Wong. Thanks also to Ben Cooksley, who managed the configuration that enabled the site’s CI/CD process, making changes seamless.

If you encounter any issues with the new website—whether technical or content-related—or have any ideas or suggestions, please feel free to <a href="{{< ref "/contact" >}}">{{< i18n "contact" >}}</a> the project.
