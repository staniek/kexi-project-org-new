---
title: "KEXI 3.0 Released"
date: "2016-10-05"
categories: 
  - "news"
  - "officialrelease"
---

__KEXI 3.0, built on KF 5 and Qt 5, introduces database improvements and a refreshed look and feel.__

## Highlights

Built on top of KF 5 and Qt 5, KEXI 3.0.0 introduces a new PostgreSQL driver, significant improvements in database migration for MS Access, MySQL, PostgreSQL, and SQLite. Most icons adopted the Breeze theme. Command-line options have been restored, issues for Date/Time values and masks fixed. The Welcome page's behavior has been refined for smoother startup. The reporting module has received numerous improvements.

KEXI 3.0 uses all the three KEXI frameworks and the version 3.0 is a minimal version required by to run the app.


## Details & Download

For detailed list of changes, and downloads, visit the development page: https://community.kde.org/Kexi/Releases#3.0.

This release offers source code downloads and installers for 64bit MS Windows. For versions for other systems ask your Linux distributor. The source code is known to build and run with Linux and MS Windows.

{{< see-download >}}

{{< see-release-history >}}
