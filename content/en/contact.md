---
title: "Contact"
date: "2024-11-24"
layout: simple
---

If you have <a href="{{< ref "file-a-bug" >}}">bugs to report</a>, suggestions, or ideas to share, join the KDE Discuss forums. You can ask questions, share your work or ideas, or see what others are talking about. You'll need a free account to post. Don't forget to use the 'kexi' tag in your posts.

{{< button url="https://discuss.kde.org" target="_blank" >}}

{{< i18n "visit-forum" >}}

{{< /button >}}

<br/>
<br/>
<br/>

Stay connected with the KEXI community through social media, chats, and mailing lists. Follow us on popular platforms for updates and announcements, join real-time conversations in chat groups, or subscribe to mailing lists for in-depth discussions and news. These channels are great for community interaction, and staying informed about the latest developments.

{{< button url="https://community.kde.org/Kexi/Contact" target="_blank" >}}

Visit Social Media, Chats, and Mailing Lists

{{< /button >}}
