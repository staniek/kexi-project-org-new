---
title: "History"
date: "2024-11-28"
layout: simple
---

KEXI development began in 2002 with the goal of bringing *Microsoft Access*-like functionality to the Free Software and Open Source community. The initiative aimed to provide a powerful yet user-friendly database management application tailored to the needs of open ecosystems. *OpenOffice Polska* (later renamed to *OpenOffice Software*) played a significant role in the early years, contributing extensively from 2003 to 2008.

Initially, KEXI was part of the broader *KOffice* project, which sought to create a comprehensive Open Source office suite. The project leveraged KDE's robust organizational structure and shared tools and technologies within the KDE ecosystem.

## Key Milestones

- ### Early Releases
  The first public beta of KEXI, version 0.1 beta 2, was unveiled on January 22, 2004. This was followed by the first stable release, version 0.9, on May 31, 2005. KEXI continued to mature with stable releases included in *KOffice* 1.5 and 1.6 during 2006.

- ### Commercial Windows Releases
  Between 2004 and 2007, OpenOffice Polska produced commercial KEXI releases for Windows, using release years as version identifiers. The initial version, Kexi 2004 LT, launched in November 2004 and was based on KEXI 0.1. The commercial line concluded with KEXI 2007.1 in March 2007, built on KEXI 1.1.2/1.1.3. These releases offered professional technical support and comprehensive documentation, much of which was later contributed back to the open project.

- ### Transition to KDE Platform 4
  The transition to KDE Platform 4 (based on Qt 4) was a significant undertaking, culminating in the release of *KOffice* 2.2 in May 2010. This version introduced features like the Report Designer plug-in. Later that year, KEXI 2.3, included in *KOffice* 2.3, added enhancements such as a redesigned Project Navigator pane.

- ### Integration into Calligra Suite
  After the 2.3 release, KEXI became part of the Calligra Suite, starting with the 2.4 release in April 2012. One of the most visible updates in this version was the introduction of the Modern Menu, a revamped main user interface.

- ### Support for Microsoft Windows Returns
  With version 3.1, KEXI reinstated official support for Microsoft Windows, reaffirming its commitment to cross-platform accessibility.

## Contributions and Community Involvement
Throughout its development, KEXI has participated in key initiatives like Google Summer of Code and KDE developer meetings. These efforts not only advanced KEXI's capabilities but also contributed to the development of KDE frameworks, benefiting numerous other Open Source applications.

{{< figure src="images/pages/kexi-history-screen.webp" title="KDE / Calligra / KEXI contributors, Berlin - 2010"
alt="KDE / Calligra / KEXI contributors, Berlin - 2010" >}}
