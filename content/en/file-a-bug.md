---
title: "File a Bug or Wish"
date: "2024-12-16"
layout: simple
---

KEXI developers do their best to bring you a stable program but with so many features and multiple platforms to support, currently Windows, Linux, and other UNIX systems. However it’s inevitable that something will go wrong somewhere.

What to do if you suspect a bug?

1. Describe the problem on a forum, chat, or send an email. See  <a href="{{< ref "/contact" >}}">{{< i18n "contact" >}}</a>.
The community will help you determine if the cause is a software bug or a problem with your device. Either way, you'll get help.
Before you hit _Submit_, please be sure to have included:
    * A complete description of the problem and if possible complete screenshot of the entire KEXI window showing the problem.
    * Your operating system (Windows, Linux, ...)
    * KEXI version number (go to (?) Help &rarr; About KEXI)

2. Once it's determined this is a bug, go ahead and make a formal bug report at [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Kexi&format=guided).

Need additional help reporting a bug? See https://community.kde.org/Kexi/File_a_bug_or_wish.

Do **not** mail the KEXI developers directly for support. KEXI has many users, and not so many developers!
