# Day-to-day Usage

This document provides instructions for non-developer tasks for the web site such as creating news posts, uploading images, or performing releases. While you can complete these tasks by pulling the code using GIT and following the Readme document, there's also an embedded web-based code editor that simplifies some of these actions. We'll explain how to use the online code editor here, but the process is almost the same if you prefer using GIT.

## Accessing the Online Code Editor

To make changes to kexi-project.org, you'll need to create an account on [invent.kde.org](https://invent.kde.org). Since the site hosts many projects, the first step is to locate the *KEXI Website* project.

![](static/readme/tutorial-1.png)

![](static/readme/tutorial-2.png)

Once you're in the project, you’ll find an "Edit" button that opens the Web IDE (the code editor) where you can make changes.

![](static/readme/tutorial-3.png)

_Note: You must be an admin of the *KEXI Website* project to push changes to the live site._

## Creating a News Post

Now that you’re in the online code editor, let's create a new post. The editor is based on Microsoft’s VSCode, so it will feel familiar if you’ve used it before. The screenshot below shows how you can see all project files by clicking the icon circled in red. This tab is usually selected by default when you open the editor.

![](static/readme/tutorial-4.png)

News posts are stored in the content folder. The "en" folder is crucial as it’s the default language, and all translations will stem from it. When the "2024" folder is selected, click the + icon circled in the screenshot below.

![](static/readme/tutorial-5.png)

A new document will be created. Enter a file name for the post. This name will appear in the URL, so avoid using spaces. News posts always end with .md (markdown files). The file will be blank, so refer to another post for the format.

To reference a 2024 post, double-click another .md file in the 2024 folder to open it in the editor.

## Understanding the Metadata at the Top of a Post

![](static/readme/tutorial-7.png)

The section at the top of the post, between three hyphens, is the front matter, which contains the post's metadata. Here’s what each element means:

- title: The title that appears at the top of the page.
- date: The release date of the post.
- draft: If set to "true", the post will not be published (e.g., draft: "true").
- hidden: The post will be published but won’t appear in the news section (e.g., hidden: "true").

The "hidden" metadata is useful for sharing work-in-progress posts without displaying them publicly. The link will still be accessible, and hidden posts are searchable, making it easier to find them.

## Making Changes to Your News Post and Saving

![](static/readme/tutorial-7.png)

Now, add content to your post. In this example, we haven’t set "hidden" to true, but that’s often the case for drafts. After making your changes, save the file.

When you make changes, a purple icon will appear on the right side, next to the fourth icon circled in red. Click the icon, as shown below.

![](static/readme/tutorial-9.png)

This will show a history of changes. When you save, add a message explaining what you’ve done. For example, "Created a new post." Then click "Commit to master" to save.

A dialog will appear. Click "Continue". This appears because saving triggers a new build for the site, updating it via the master branch.

![](static/readme/tutorial-10.png)

A message will appear in the bottom right, taking you back to the project.

![](static/readme/tutorial-11.png)


Once you're in the project, you’ll find an "Edit" button that opens the Web IDE (the code editor) where you can make changes.



Note: You must be an admin of the kexi-project.org project to push changes to the live site.

Creating a News Post
Now that you’re in the online code editor, let's create a new post. The editor is based on Microsoft’s VSCode, so it will feel familiar if you’ve used it before. The screenshot below shows how you can see all project files by clicking the icon circled in red. This tab is usually selected by default when you open the editor.



News posts are stored in the content folder. The en folder is crucial as it’s the default language, and all translations will stem from it. When the "2024" folder is selected, click the + icon circled in the screenshot below.



A new document will be created. Enter a file name for the post. This name will appear in the URL, so avoid using spaces. News posts always end with .md (markdown files). The file will be blank, so refer to another post for the format.

To reference a 2024 post, double-click another .md file in the 2024 folder to open it in the editor.

Understanding the Metadata at the Top of a Post


The section at the top of the post, between three hyphens, is the front matter, which contains the post's metadata. Here’s what each element means:

title: The title that appears at the top of the page.
date: The release date of the post.
draft: If set to "true", the post will not be published (e.g., draft: "true").
hidden: The post will be published but won’t appear in the news section (e.g., hidden: "true").
The "hidden" metadata is useful for sharing work-in-progress posts without displaying them publicly. The link will still be accessible, and hidden posts are searchable, making it easier to find them.

Making Changes to Your News Post and Saving


Now, add content to your post. In this example, we haven’t set "hidden" to true, but that’s often the case for drafts. After making your changes, save the file.

When you make changes, a purple icon will appear on the right side, next to the fourth icon circled in red. Click the icon, as shown below.



This will show a history of changes. When you save, add a message explaining what you’ve done. For example, "Created a new post." Then click "Commit to master" to save.

A dialog will appear. Click "Continue". This appears because saving triggers a new build for the site, updating it via the master branch.



A message will appear in the bottom right, taking you back to the project.



Back in the project view, you’ll see your new message at the top, indicating it was the most recent save/commit.

![](static/readme/tutorial-12.png)

Saving triggers a new build. To track progress, go to "Build" > "Pipelines" on the left side and click it. Once the build is complete, you’ll see a green checkmark.

![](static/readme/tutorial-13.png)

Congratulations! Your new post is live on the site.

## Uploading Images

You can upload images to the static folder. Files in this folder are published when the site builds. It’s a good idea to keep things organized in subfolders. To upload, right-click within the editor as shown below.

![](static/readme/tutorial-14.png)

You can add images using markdown, following this format. A [cheat sheet](https://www.markdownguide.org/cheat-sheet/) is available for markdown syntax, or you can refer to other posts for examples.

## New Releases

For new releases, in addition to creating a news post, you need to update version numbers and links on the download page. This information is stored in the __data > releases.yaml__ file, where you can update versions and URLs for the binary files.

If more manual updates are needed on the download page, check the __layouts > \_default > download.html__ file for changes.

# Creating Redirects

Redirects are managed in the .htaccess file located in the "static" folder. You can view examples of existing redirects within this file for reference.
